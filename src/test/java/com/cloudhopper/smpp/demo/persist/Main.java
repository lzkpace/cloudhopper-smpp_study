package com.cloudhopper.smpp.demo.persist;

/*
 * #%L
 * ch-smpp
 * %%
 * Copyright (C) 2009 - 2014 Cloudhopper by Twitter
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import cn.hutool.db.Session;
import com.cloudhopper.commons.charset.CharsetUtil;
import com.cloudhopper.commons.charset.GSMCharset;
import com.cloudhopper.commons.util.*;
import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.tlv.Tlv;
import com.cloudhopper.smpp.type.*;
import com.google.common.util.concurrent.RateLimiter;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);


    public static RateLimiter rateLimiter = RateLimiter.create(100);

    public static void main(String[] args) throws IOException, RecoverablePduException, InterruptedException,
            SmppChannelException, UnrecoverablePduException, SmppTimeoutException {
        DummySmppClientMessageService smppClientMessageService = new DummySmppClientMessageService();
        int i = 1;
        final LoadBalancedList<OutboundClient> balancedList = LoadBalancedLists.synchronizedList(new RoundRobinLoadBalancedList<OutboundClient>());
        for (; i <= 3; i++) {
//			Thread.sleep(500);
            balancedList.set(createClient(smppClientMessageService, i), 1);
        }
        final boolean messagePayload = false;


        final ExecutorService executorService = Executors.newFixedThreadPool(10);
        String input = null;
        while (true) {
            Scanner terminalInput = new Scanner(System.in);
            input = terminalInput.next();
            if (!StringUtil.isEmpty(input)) {
//                balancedList.getNext().getSession().unbind(5000);
                final long messagesToSend = 1000;
                for (int j = 0; j < messagesToSend; j++) {
                    String finalInput = input;
                    executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                rateLimiter.acquire(1);
                                System.out.println(Thread.currentThread().getName());
                                System.out.println("----------------------------");
                                final OutboundClient next = balancedList.getNext();
                                final SmppSession session = next.getSession();
                                if (session != null && session.isBound()) {
                                    //内容
//                                    String body = "your inv 123456";
//                                    String body = "pinjaman The deposit method is updated again, https://is.gd/rFdheYjust to make you satisfied and discover more wonderful things, start from here.,123123";
                                    String body = "fdsafdafdafdsfdafasfdasfadsfasfsad1 123445العمل من المنزل،الدخل اليومي 1K-7Kلبرجاء النقر فوق: wa.me/85294265839";
                                    String from = "test";
                                    //接收方号码
//                                    String address = "91" + returnRandomConfNumber(10);
                                    String address = "+918925432780";
                                    if (finalInput.length() > 5) {
                                        address = terminalInput.next();
                                    }
                                    String entity_id_value = "";
                                    String template_id_value = "";
                                    short pe_id = 0x1400;
                                    short template_id = 0x1401;
                                    byte[] textBytes = null;
                                    boolean GSMRepresent = GSMCharset.canRepresent(body);
                                    if (!GSMRepresent) {
                                        textBytes = CharsetUtil.encode(body, CharsetUtil.CHARSET_UCS_2);
                                    } else {
                                        textBytes = CharsetUtil.encode(body, CharsetUtil.CHARSET_GSM);
                                    }
                                    int length = 160;
                                    if (!GSMRepresent) {
                                        length = 140;
                                    }
                                    SubmitSmResp submitResp = null;
                                    if (textBytes != null && textBytes.length <= length) {
                                        SubmitSm submit = new SubmitSm();
                                        submit.setSourceAddress(new Address((byte) 0x03, (byte) 0x01, from));
                                        submit.setDestAddress(new Address((byte) 0x01, (byte) 0x01, address));
                                        submit.setRegisteredDelivery(SmppConstants.REGISTERED_DELIVERY_SMSC_RECEIPT_REQUESTED);
                                        submit.setShortMessage(textBytes);
                                        if (!GSMRepresent) {
                                            submit.setDataCoding(SmppConstants.DATA_CODING_UCS2);
                                        }
                                        if (!StringUtil.isEmpty(entity_id_value)) {
                                            submit.addOptionalParameter(new Tlv(pe_id, CharsetUtil.encode(entity_id_value, CharsetUtil.CHARSET_GSM), ""));
                                        }
                                        if (!StringUtil.isEmpty(template_id_value)) {
                                            submit.addOptionalParameter(new Tlv(template_id, CharsetUtil.encode(template_id_value, CharsetUtil.CHARSET_GSM), ""));
                                        }
                                        submitResp = session.submit(submit, 100000);
                                        logger.info("[短信提交/单条]submitResp:{}", submitResp);
                                    } else {
                                        if (messagePayload) {
//                                            //附加消息提交,内容计费根据通道方定义,单条长短信提交,160字符计一条
//                                            Tlv tlv = new Tlv(SmppConstants.TAG_MESSAGE_PAYLOAD,textBytes);
//                                            SubmitSm submitSm = new SubmitSm();
//                                            submitSm.setSourceAddress(new Address((byte) 0x00, (byte) 0x01, from));
//                                            submitSm.setDestAddress(new Address((byte)0x00, (byte)0x01, address));
//                                            submitSm.setRegisteredDelivery(SmppConstants.REGISTERED_DELIVERY_SMSC_RECEIPT_REQUESTED);
//                                            if(!GSMRepresent) {
//                                                submitSm.setDataCoding(SmppConstants.DATA_CODING_UCS2);
//                                            }

                                            int maximumMultipartMessageSegmentSize = 160;
                                            if (!GSMRepresent) {
                                                maximumMultipartMessageSegmentSize = 140;
                                            }

                                            byte[][] byteMessagesArray = splitUnicodeMessage(false, textBytes, maximumMultipartMessageSegmentSize);
                                            //短信引用序号
                                            Tlv newSarMsgRefNum = new Tlv(SmppConstants.TAG_SAR_MSG_REF_NUM, ByteArrayUtil.toByteArray((short) new Random().nextInt()));
                                            //分片总数
                                            Tlv sarTotalSegments = new Tlv(SmppConstants.TAG_SAR_TOTAL_SEGMENTS, ByteArrayUtil.toByteArray((short) 3));
                                            for (int k = 0; k < byteMessagesArray.length; k++) {
                                                //当前序列
                                                Tlv sarSegmentSeqnum = new Tlv(SmppConstants.TAG_SAR_SEGMENT_SEQNUM, ByteArrayUtil.toByteArray(Short.parseShort(k + 1 + "")));
                                                SubmitSm submitSm = new SubmitSm();
                                                submitSm.setSourceAddress(new Address((byte) 0x03, (byte) 0x01, from));
                                                submitSm.setDestAddress(new Address((byte) 0x01, (byte) 0x01, address));
                                                submitSm.setRegisteredDelivery(SmppConstants.REGISTERED_DELIVERY_SMSC_RECEIPT_REQUESTED);
                                                submitSm.setOptionalParameter(newSarMsgRefNum);
                                                submitSm.setOptionalParameter(sarSegmentSeqnum);
                                                submitSm.setOptionalParameter(sarTotalSegments);
                                                submitSm.setShortMessage(byteMessagesArray[k]);
                                                submitResp = session.submit(submitSm, 20000);
                                                logger.info("单条长短信提交,{},{}", submitSm, submitResp);

                                            }

                                            logger.info("码{}", SmppConstants.STATUS_MESSAGE_MAP.get(submitResp.getCommandStatus()));

                                        } else {
                                            int maximumMultipartMessageSegmentSize = 153;
                                            if (!GSMRepresent) {
                                                maximumMultipartMessageSegmentSize = 134;
                                            }
                                            byte[][] byteMessagesArray = splitUnicodeMessage(true, textBytes, maximumMultipartMessageSegmentSize);
                                            int count = byteMessagesArray.length;

                                            // submit all messages
                                            for (int j = 0; j < count; j++) {
                                                SubmitSm submit0 = new SubmitSm();
                                                if (count > 1) {
                                                    submit0.setEsmClass(SmppConstants.ESM_CLASS_UDHI_MASK);
                                                }
                                                submit0.setSourceAddress(new Address((byte) 0x03, (byte) 0x01, from));
                                                submit0.setDestAddress(new Address((byte) 0x01, (byte) 0x01, address));
                                                submit0.setRegisteredDelivery(SmppConstants.REGISTERED_DELIVERY_SMSC_RECEIPT_REQUESTED);
                                                submit0.setShortMessage(byteMessagesArray[j]);
                                                if (!GSMRepresent) {
                                                    submit0.setDataCoding(SmppConstants.DATA_CODING_UCS2);
                                                }
//                                                if (!StringUtil.isEmpty(entity_id_value)) {
//                                                    submit0.addOptionalParameter(new Tlv(pe_id, CharsetUtil.encode(entity_id_value, CharsetUtil.CHARSET_GSM), ""));
//                                                }
//                                                if (!StringUtil.isEmpty(template_id_value)) {
//                                                    submit0.addOptionalParameter(new Tlv(template_id, CharsetUtil.encode(template_id_value, CharsetUtil.CHARSET_GSM), ""));
//                                                }
                                                if (j + 1 == count) {
                                                    submitResp = session.submit(submit0, 60000);
                                                } else {
                                                    session.sendRequestPdu(submit0, 0, false);
                                                }
//                                                Thread.sleep(1000L);
                                                logger.info("[短信发送/分片]+[{}] sendWindow.size: {}, receiver:{},index:[{}],submitResp:{}", j, session.getSendWindow().getSize(), address, j, submitResp);
                                            }
                                            String result = submitResp.getResultMessage();
                                            String messageId = submitResp.getMessageId();
                                            logger.info("px:systemId:{},from:{},to:{},result:{},realMessageId:{}",
                                                    session.getConfiguration().getSystemId(),
                                                    from,
                                                    address,
                                                    result,
                                                    messageId);

                                        }

                                    }
                                    Assert.assertNotNull(submitResp);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.err.println(e.toString());
                            }
                        }
                    });
                }
            }
        }
//		executorService.shutdownNow();
//		ReconnectionDaemon.getInstance().shutdown();
//		for (LoadBalancedList.Node<OutboundClient> node : balancedList.getValues()) {
//			node.getValue().shutdown();
//		}
    }

    private static String returnRandomConfNumber(int length) {
        Random random = new Random();
        return String.valueOf(random.nextLong()).substring(1, length + 1);
    }

    private static OutboundClient createClient(DummySmppClientMessageService smppClientMessageService, int i) {
        OutboundClient client = new OutboundClient();
        client.initialize(getSmppSessionConfiguration(i), smppClientMessageService);
        client.scheduleReconnect();
        return client;
    }

//    private static SmppSessionConfiguration getSmppSessionConfiguration(int i) {
//        SmppSessionConfiguration config = new SmppSessionConfiguration();
//        config.setWindowSize(10);
//        config.setName("Tester.Session." + i);
//        config.setType(SmppBindType.TRANSCEIVER);
////        config.setHost("192.168.0.33");
//        config.setHost("127.0.0.1");
//        config.setPort(7776);
//        config.setConnectTimeout(10000);
//        config.setSystemId("KMI62mk");
//        config.setPassword("1234");
//        config.getLoggingOptions().setLogBytes(false);
//        // to enable monitoring (request expiration)
//        config.setInterfaceVersion(SmppConstants.VERSION_3_4);
//        config.setRequestExpiryTimeout(30000);
//        config.setWindowMonitorInterval(15000);
//        config.getLoggingOptions().setLogPdu(true);
//        config.setCountersEnabled(true);
//        return config;
//    }


    private static SmppSessionConfiguration getSmppSessionConfiguration(int i) {
        SmppSessionConfiguration config = new SmppSessionConfiguration();
        config.setWindowSize(2000);
        config.setName("Tester.Session." + i);
        config.setType(SmppBindType.TRANSMITTER);
        config.setSystemType("VMA");
        config.setHost("smpp.revicom.ltd");
        config.setPort(2777);
        config.setConnectTimeout(10000);
        config.setSystemId("mobiprima_smpp_dir");
        config.setPassword("axaiG7ae");
        config.setSystemType("TOC");
        config.getLoggingOptions().setLogBytes(false);
        config.setInterfaceVersion(SmppConstants.VERSION_3_4);
        config.setRequestExpiryTimeout(30000);
        config.setWindowMonitorInterval(15000);
        config.setCountersEnabled(true);
        return config;
    }


    /**
     * 短信拆分
     *
     * @param aMessage
     * @param maximumMultipartMessageSegmentSize
     * @return
     */
    private static byte[][] splitUnicodeMessage(boolean hasUdh, byte[] aMessage, Integer maximumMultipartMessageSegmentSize) {
        final byte UDHIE_HEADER_LENGTH = 0x05;
        final byte UDHIE_IDENTIFIER_SAR = 0x00;
        final byte UDHIE_SAR_LENGTH = 0x03;

        // determine how many messages have to be sent
        int numberOfSegments = aMessage.length / maximumMultipartMessageSegmentSize;
        int messageLength = aMessage.length;
        if (numberOfSegments > 255) {
            numberOfSegments = 255;
            messageLength = numberOfSegments * maximumMultipartMessageSegmentSize;
        }
        if ((messageLength % maximumMultipartMessageSegmentSize) > 0) {
            numberOfSegments++;
        }

        // prepare array for all of the msg segments
        byte[][] segments = new byte[numberOfSegments][];

        int lengthOfData;

        // generate new reference number
        byte[] referenceNumber = new byte[1];
        new Random().nextBytes(referenceNumber);

        // split the message adding required headers
        for (int i = 0; i < numberOfSegments; i++) {
            if (numberOfSegments - i == 1) {
                lengthOfData = messageLength - i * maximumMultipartMessageSegmentSize;
            } else {
                lengthOfData = maximumMultipartMessageSegmentSize;
            }
            // new array to store the header
            segments[i] = new byte[6 + lengthOfData];
            if (hasUdh) {
                // UDH header
                // doesn't include itself, its header length
                segments[i][0] = UDHIE_HEADER_LENGTH;
                // SAR identifier
                segments[i][1] = UDHIE_IDENTIFIER_SAR;
                // SAR length
                segments[i][2] = UDHIE_SAR_LENGTH;
                // reference number (same for all messages)
                segments[i][3] = referenceNumber[0];
                // total number of segments
                segments[i][4] = (byte) numberOfSegments;
                // segment number
                segments[i][5] = (byte) (i + 1);
                // copy the data into the array
                System.arraycopy(aMessage, (i * maximumMultipartMessageSegmentSize), segments[i], 6, lengthOfData);
            } else {
                System.arraycopy(aMessage, (i * maximumMultipartMessageSegmentSize), segments[i], 0, lengthOfData);
            }

        }
        return segments;
    }


    private static String submitMessage(Session session, byte esmClass, Address sourceAddress, Address destAddress, byte registeredDelivery, byte dataCoding, Tlv... optionalParameter) {

        return null;
    }

}
