package com.cloudhopper.smpp.demo.persist;

/*
 * #%L
 * ch-smpp
 * %%
 * Copyright (C) 2009 - 2014 Cloudhopper by Twitter
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.cloudhopper.commons.charset.CharsetUtil;
import com.cloudhopper.commons.util.LoadBalancedList;
import com.cloudhopper.commons.util.LoadBalancedLists;
import com.cloudhopper.commons.util.RoundRobinLoadBalancedList;
import com.cloudhopper.commons.util.StringUtil;
import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.tlv.Tlv;
import com.cloudhopper.smpp.type.*;
import com.cloudhopper.smpp.util.Concatenation;
import com.cloudhopper.smpp.util.Gsm0338;
import com.cloudhopper.smpp.util.Ucs2;
import com.google.common.util.concurrent.RateLimiter;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 1. shoutdown = true ,标志断开连接,重连动作不再继续
 * 2. disconnect() 执行 ,主要针对smppSession 进行操作,取消其enquireLinkTask心跳定时任务.
 * 3. destroySession();session判空,如果开启了counters ,计数统计则销毁前可进行归档汇总,打印.
 * 4. session 销毁,将session 状态置为un-binding ,即解除绑定的过程中(该状态可能来自于客户端也可能由服务端发起)改变其 netty 中 channel 的状态(修改前先判定是处于已连接状态),channel 执行close()方法.不论执行结果都将 session 的状态置为 close 关闭状态;
 * 5. window执行 destroy ()方法继续销毁,并且取消一切正在进行的请求以及等待响应的监听队列以及监听任务.
 * 6. 确认连接彻底废弃后,将sessionHandler ,将接收消息处理器的引用置为 null, 避免循环引用释放空间;
 * 7. 关闭 clientBootstrap 客户端连接引导程序
 * 8. 销毁所有涉及异步执行以及监控的线程池;
 *
 * 所谓’优雅’断开时则需要调整上面的执行步骤,主要不同点在第4步,其旨在客户端需要断连时需向服务端发送询问性质的断连请求,其结果可能是’可以断开’或者’存在未完成请求,无法断开’,客户端可根据其响应结果确定是否执行后续的解绑操作,在得到肯定的响应之前不应该主动停止有关正常连接需进行的一切操作;
 */
public class Main3 {
    private static final Logger logger = LoggerFactory.getLogger(Main3.class);

    public static RateLimiter rateLimiter = RateLimiter.create(100);
    private static final Random RANDOM = new Random();

    private static OutboundClient initPersistClient(int i) {

        DummySmppClientMessageService smppClientMessageService = new DummySmppClientMessageService();

        OutboundClient outboundClient = createClient(smppClientMessageService, i);

        return outboundClient;
    }

    public static void main(String[] args) throws IOException, RecoverablePduException, InterruptedException,
            SmppChannelException, UnrecoverablePduException, SmppTimeoutException {
        DummySmppClientMessageService smppClientMessageService = new DummySmppClientMessageService();

        final LoadBalancedList<OutboundClient> balancedList = LoadBalancedLists.synchronizedList(new RoundRobinLoadBalancedList<OutboundClient>());
        for ( int i = 1;i <5; i++) {
            balancedList.set(createClient(smppClientMessageService, i), 1);
        }

        final ExecutorService executorService = Executors.newFixedThreadPool(10);
        while (true) {
            String input = null;
            Scanner terminalInput = new Scanner(System.in);
            input = terminalInput.next();
            if (!StringUtil.isEmpty(input)) {
                //断开与连接同时进行
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        for ( int i = 1;i <5; i++) {
                            balancedList.set(createClient(smppClientMessageService, i), 1);
                        }
                    }
                });
                for (int i= 0;i<balancedList.getSize();i++) {
                    executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            OutboundClient value = balancedList.getNext();
                            value.shutdown();
                        }
                    });
                }
            }
        }
    }

    private static String returnRandomConfNumber(int length) {
        Random random = new Random();
        return String.valueOf(random.nextLong()).substring(1, length + 1);
    }

    private static OutboundClient createClient(DummySmppClientMessageService smppClientMessageService, int i) {
        OutboundClient client = new OutboundClient();
        client.initialize(getSmppSessionConfiguration(i), smppClientMessageService);
        client.scheduleReconnect();
        return client;
    }

    private static SmppSessionConfiguration getSmppSessionConfiguration(int i) {
        SmppSessionConfiguration config = new SmppSessionConfiguration();
        config.setWindowSize(1000);
        config.setName("Tester.Session." + i);
        config.setType(SmppBindType.TRANSCEIVER);
//        config.setHost("47.241.124.164");
//        config.setHost("192.168.0.165");
        config.setHost("127.0.0.1");
        config.setPort(7776);
        config.setConnectTimeout(10000);
        config.setSystemId("KMI62mk");
//        config.setSystemId("YFOT52");
        config.setPassword("1234");
        config.getLoggingOptions().setLogBytes(false);
        // to enable monitoring (request expiration)
        config.setInterfaceVersion(SmppConstants.VERSION_3_4);
        config.setRequestExpiryTimeout(30000);
        config.setWindowMonitorInterval(15000);
        config.setCountersEnabled(true);
        return config;
    }

//    private static SmppSessionConfiguration getSmppSessionConfiguration(int i) {
//        SmppSessionConfiguration config = new SmppSessionConfiguration();
//        config.setWindowSize(100000);
//        config.setName("Tester.Session." + i);
//        config.setType(SmppBindType.TRANSCEIVER);
//        config.setHost("47.241.3.118");
//        config.setPort(7776);
//        config.setConnectTimeout(10000);
//        config.setSystemId("test-mk-91");
//        config.setPassword("888888");
//        config.getLoggingOptions().setLogBytes(false);
//        config.setUseSsl(true);
//        config.setSslConfiguration(null);
//        // to enable monitoring (request expiration)
//        config.setInterfaceVersion(SmppConstants.VERSION_3_4);
//        config.setRequestExpiryTimeout(30000);
//        config.setWindowMonitorInterval(15000);
//        config.setCountersEnabled(true);
//        return config;
//    }


    /**
     * 短信内容分片拆分
     *
     * @param session
     * @param message
     * @param esmClass
     * @param sourceAddress
     * @param destAddress
     * @param registeredDelivery
     * @param dataCoding
     * @param optionalParameter
     * @return
     */
    private static String submitMessage(SmppSession session, byte[] message, byte esmClass, Address sourceAddress, Address destAddress, byte registeredDelivery, byte dataCoding, Tlv... optionalParameter) {
        SubmitSm sms = new SubmitSm();
        sms.setEsmClass(esmClass);
        sms.setSourceAddress(sourceAddress);
        sms.setDestAddress(destAddress);
        sms.setRegisteredDelivery(registeredDelivery);
        sms.setDataCoding(dataCoding);
        try {
            sms.setShortMessage(message);
        } catch (SmppInvalidArgumentException e) {
            e.printStackTrace();
        }
        for (Tlv optional : optionalParameter) {
            sms.setOptionalParameter(optional);
        }
        try {
            SubmitSmResp submitResp = session.submit(sms, 400000L);
            logger.info("Message submitted, message_id is {}", submitResp.getMessageId());
            Assert.assertNotNull(submitResp);
        } catch (RecoverablePduException e) {
            e.printStackTrace();
        } catch (UnrecoverablePduException e) {
            e.printStackTrace();
        } catch (SmppTimeoutException e) {
            //提交超时
            e.printStackTrace();
        } catch (SmppChannelException e) {
            //通道连接异常
            e.printStackTrace();
        } catch (InterruptedException e) {
            //异常中断
            e.printStackTrace();
        }
        return null;
    }

}
