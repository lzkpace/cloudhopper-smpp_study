package com.cloudhopper.smpp.demo.persist;

/*
 * #%L
 * ch-smpp
 * %%
 * Copyright (C) 2009 - 2014 Cloudhopper by Twitter
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.cloudhopper.commons.util.DateTimeUtil;
import com.cloudhopper.commons.util.StringUtil;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.impl.DefaultSmppSessionHandler;
import com.cloudhopper.smpp.pdu.*;
import com.cloudhopper.smpp.util.DateFormatUtil;
import com.cloudhopper.smpp.util.DeliveryReceipt;
import com.cloudhopper.smpp.util.DeliveryReceiptException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ClientSmppSessionHandler extends DefaultSmppSessionHandler {

	private static final Logger logger = LoggerFactory.getLogger(ClientSmppSessionHandler.class);

	private OutboundClient client;
	private SmppClientMessageService smppClientMessageService;

	public ClientSmppSessionHandler(OutboundClient client,
									SmppClientMessageService smppClientMessageService) {
		super(logger);
		this.client = client;
		this.smppClientMessageService = smppClientMessageService;
	}

	@Override
	public void firePduRequestExpired(PduRequest pduRequest) {
		logger.warn("PDU request expired: {}", pduRequest);
	}

	@Override
	public PduResponse firePduRequestReceived(PduRequest request) {
		PduResponse response;
		try {
			if (request instanceof DeliverSm) {
				logger.info("接收 DR {}", request);
				response = smppClientMessageService.received(client, (DeliverSm) request);
				DeliverSm deliverSm = ((DeliverSm) request);
				DeliverSmResp deliverSmResp = (DeliverSmResp) response;

				String DR_MESSAGE = new String(deliverSm.getShortMessage(), StandardCharsets.ISO_8859_1);
				DeliveryReceipt dlr=new DeliveryReceipt();
				try {
					dlr = DeliveryReceipt.parseShortMessage(DR_MESSAGE, DateTimeZone.forID("+08:00"), false);
				} catch (DeliveryReceiptException e) {
					e.printStackTrace();
					logger.warn("DeliveryReceipt解析存在异常DeliverSm:[{}]",DR_MESSAGE);
				}

				String messageId = dlr.getMessageId();
				deliverSmResp.setMessageId(messageId);
				DateTime submitTime = dlr.getSubmitDate().toDateTime(DateTimeZone.forID("+08:00"));
				DateTime doneDate = dlr.getDoneDate();
				String errCode = String.valueOf(dlr.getErrorCode());
				String receiver = deliverSm.getSourceAddress().getAddress();
				String messageStatus = DeliveryReceipt.toStateText(dlr.getState());
				logger.info("DeliverReceipt 字符串解析:[messageId:{},submitTime:{},endTime:{},errCode:{},receiver:{},messageStatus:{}]",messageId,submitTime,doneDate,errCode,receiver,messageStatus);
			} else {
				response = request.createResponse();
			}
		} catch (Throwable e) {
			LoggingUtil.log(logger, e);
			response = request.createResponse();
			response.setResultMessage(e.getMessage());
			response.setCommandStatus(SmppConstants.STATUS_UNKNOWNERR);
		}
		return response;
	}

	/**
	 * TODO not sure if we really need to call reconnect here
	 */
	@Override
	public void fireUnknownThrowable(Throwable t) {
		if (t instanceof ClosedChannelException) {
			logger.warn("Unknown throwable received, but it was a ClosedChannelException, executing reconnect" + " "
					+ LoggingUtil.toString(client.getConfiguration()));
			client.scheduleReconnect();
		} else if (t instanceof IOException) {
			logger.warn(t + " " + LoggingUtil.toString(client.getConfiguration()));
			//#fireChannelUnexpectedlyClosed will be called from a different place
		} else {
			logger.warn(String.valueOf(t) + " " + LoggingUtil.toString(client.getConfiguration()), t);
		}
	}

	@Override
	public void fireChannelUnexpectedlyClosed() {
		client.scheduleReconnect();
	}

}
