package com.cloudhopper.smpp.demo.persist;

/*
 * #%L
 * ch-smpp
 * %%
 * Copyright (C) 2009 - 2014 Cloudhopper by Twitter
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.cloudhopper.commons.charset.CharsetUtil;
import com.cloudhopper.commons.util.LoadBalancedList;
import com.cloudhopper.commons.util.LoadBalancedLists;
import com.cloudhopper.commons.util.RoundRobinLoadBalancedList;
import com.cloudhopper.commons.util.StringUtil;
import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.tlv.Tlv;
import com.cloudhopper.smpp.type.*;
import com.cloudhopper.smpp.util.Concatenation;
import com.cloudhopper.smpp.util.Gsm0338;
import com.cloudhopper.smpp.util.Ucs2;
import com.google.common.util.concurrent.RateLimiter;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main2 {
    private static final Logger logger = LoggerFactory.getLogger(Main2.class);

    public static RateLimiter rateLimiter = RateLimiter.create(100);
    private static final Random RANDOM = new Random();

    private static OutboundClient initPersistClient(int i) {

        DummySmppClientMessageService smppClientMessageService = new DummySmppClientMessageService();

        OutboundClient outboundClient = createClient(smppClientMessageService, i);

        return outboundClient;
    }

    public static void main(String[] args) throws IOException, RecoverablePduException, InterruptedException,
            SmppChannelException, UnrecoverablePduException, SmppTimeoutException {
        DummySmppClientMessageService smppClientMessageService = new DummySmppClientMessageService();

        final LoadBalancedList<OutboundClient> balancedList = LoadBalancedLists.synchronizedList(new RoundRobinLoadBalancedList<OutboundClient>());
        for ( int i = 1;i <5; i++) {
            balancedList.set(createClient(smppClientMessageService, i), 1);
        }
        OutboundClient outboundClient = initPersistClient(1);


        final ExecutorService executorService = Executors.newSingleThreadExecutor();
        final ExecutorService executorForPDU = Executors.newFixedThreadPool(100);

        String input = null;
        while (true) {
            int j = 0;
            Scanner terminalInput = new Scanner(System.in);
            input = terminalInput.next();
            if (!StringUtil.isEmpty(input)) {
                final long messagesToSend = 1;
                for (; j < messagesToSend; j++) {
                    String finalInput = input;
                    logger.info("messagedToSend:{}",j);
                    executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                //内容
//                                String body = "이용정보 \uD835\uDDDE\uD835\uDDE5\uD835\uDDEA 687,000 코드번호 2749 처리되었습니다 본인사용 아닐시 문의 02-6339-0701";
                                    String body = "[TeenPatti vungo] 12345 A high amount is about to expire, get it now and make your life better! https://bit.ly/o751 DOT\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000";
//                                String body = "고객님\n" +
//                                        "승인완료됐습니다\n" +
//                                        "금액 \uD835\uDFEF\uD835\uDFF3\uD835\uDFED,\uD835\uDFEF\uD835\uDFEC\uD835\uDFEC원 \n" +
//                                        "승인코드 **\uD835\uDFEF\uD835\uDFF2 \n" +
//                                        "본인아닐시 \n" +
//                                        "문의\n" +
//                                        "1660-2916";
                                String from = "CREAP";
                                //接收方号码
//                                    String address = "91" + returnRandomConfNumber(10);
                                String address = "918925432780";
//                                String address = "639669917662";
                                if (finalInput.length() > 5) {
                                    address = terminalInput.next();
                                }
                                String entity_id_value = "";
                                String template_id_value = "";
                                short pe_id = 0x1400;
                                short template_id = 0x1401;
                                byte[] textBytes = null;
                                byte[][] messages = new byte[0][];
                                byte dataCoding;
                                byte esmClass;
                                Address sourceMsisdn = new Address(SmppConstants.TON_UNKNOWN, SmppConstants.NPI_UNKNOWN, from);
                                Address destinationMsisdn = new Address(SmppConstants.TON_NATIONAL, SmppConstants.NPI_UNKNOWN, address);
                                if (Gsm0338.isBasicEncodeable(body)) {
                                    //GSM03.38 编码
                                    dataCoding = SmppConstants.DATA_CODING_DEFAULT;
                                    if (Gsm0338.countSeptets(body) > Concatenation.MAX_SINGLE_MSG_CHAR_SIZE_7BIT) {
                                        //拆分
                                        messages = Concatenation.splitGsm7bitWith8bitReference(body, CharsetUtil.CHARSET_GSM, RANDOM.nextInt());
                                        esmClass = SmppConstants.ESM_CLASS_UDHI_MASK;
                                    } else {
                                        messages = new byte[][]{CharsetUtil.encode(body, CharsetUtil.CHARSET_GSM)};
                                        esmClass = SmppConstants.ESM_CLASS_MM_DEFAULT;

                                    }
                                } else if (Ucs2.isUcs2Encodable(body)) {
                                    //UCS_2 编码
                                    dataCoding = SmppConstants.DATA_CODING_UCS2;
                                    if (body.length() > Concatenation.MAX_SINGLE_MSG_CHAR_SIZE_UCS2) {
                                        //拆分
                                        messages = Concatenation.splitUcs216bit(body, RANDOM.nextInt());
                                        esmClass = SmppConstants.ESM_CLASS_UDHI_MASK;
                                    } else {
                                        messages = new byte[][]{CharsetUtil.encode(body, CharsetUtil.CHARSET_UCS_2)};
                                        esmClass = SmppConstants.ESM_CLASS_MM_DEFAULT;
                                    }
                                } else {
                                    dataCoding =  SmppConstants.DATA_CODING_DEFAULT;
                                    esmClass = SmppConstants.ESM_CLASS_MM_DEFAULT;
                                    //不支持的文本内容
                                    logger.error("The message '{}' contains non-encodeable characters", body);
                                }

                                logger.info("sending--{}", body);
                                logger.info("Message is {} characters long and will be sent as {} messages with data coding {}",
                                        body.length(), messages.length, dataCoding);
                                // submit all messages
//                                    for (int i = messages.length-1; i >= 0; i--) {
//                                        OutboundClient next = balancedList.getNext();
//                                        SmppSession session = next.getSession();
//                                        rateLimiter.acquire(1);
//                                        String messageId = submitMessage(session, messages[i], esmClass,sourceMsisdn, destinationMsisdn,SmppConstants.REGISTERED_DELIVERY_SMSC_RECEIPT_REQUESTED
//                                                ,dataCoding);
//                                    }
                                //多线程下的乱序提交
                                for (byte[] message : messages) {
                                    executorForPDU.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            rateLimiter.acquire(1);
                                            OutboundClient next = balancedList.getNext();
                                            SmppSession session = next.getSession();
                                            submitMessage(session, message, esmClass, sourceMsisdn, destinationMsisdn, SmppConstants.REGISTERED_DELIVERY_SMSC_RECEIPT_REQUESTED
                                                    , dataCoding);
                                        }
                                    });
                                }

                            } catch (Exception e) {
                                System.err.println(e.toString());
                            }finally{
                                return;
                            }
                        }
                    });

                }
            }
        }
//		executorService.shutdownNow();
//		ReconnectionDaemon.getInstance().shutdown();
//		for (LoadBalancedList.Node<OutboundClient> node : balancedList.getValues()) {
//			node.getValue().shutdown();
//		}
    }

    private static String returnRandomConfNumber(int length) {
        Random random = new Random();
        return String.valueOf(random.nextLong()).substring(1, length + 1);
    }

    private static OutboundClient createClient(DummySmppClientMessageService smppClientMessageService, int i) {
        OutboundClient client = new OutboundClient();
        client.initialize(getSmppSessionConfiguration(i), smppClientMessageService);
        client.scheduleReconnect();
        return client;
    }

    private static SmppSessionConfiguration getSmppSessionConfiguration(int i) {
        SmppSessionConfiguration config = new SmppSessionConfiguration();
        config.setWindowSize(1000);
        config.setName("Tester.Session." + i);
        config.setType(SmppBindType.TRANSCEIVER);
//        config.setHost("47.241.124.164");
//        config.setHost("192.168.0.165");
        config.setHost("47.241.3.118");
        config.setPort(7776);
        config.setConnectTimeout(10000);
        config.setSystemId("test-mk-91");
//        config.setSystemId("YFOT52");
        config.setPassword("888888");
        config.getLoggingOptions().setLogBytes(false);
        // to enable monitoring (request expiration)
        config.setInterfaceVersion(SmppConstants.VERSION_3_4);
        config.setRequestExpiryTimeout(30000);
        config.setWindowMonitorInterval(15000);
        config.setCountersEnabled(true);
        return config;
    }

//    private static SmppSessionConfiguration getSmppSessionConfiguration(int i) {
//        SmppSessionConfiguration config = new SmppSessionConfiguration();
//        config.setWindowSize(100000);
//        config.setName("Tester.Session." + i);
//        config.setType(SmppBindType.TRANSCEIVER);
//        config.setHost("47.241.3.118");
//        config.setPort(7776);
//        config.setConnectTimeout(10000);
//        config.setSystemId("test-mk-91");
//        config.setPassword("888888");
//        config.getLoggingOptions().setLogBytes(false);
//        config.setUseSsl(true);
//        config.setSslConfiguration(null);
//        // to enable monitoring (request expiration)
//        config.setInterfaceVersion(SmppConstants.VERSION_3_4);
//        config.setRequestExpiryTimeout(30000);
//        config.setWindowMonitorInterval(15000);
//        config.setCountersEnabled(true);
//        return config;
//    }


    /**
     * 短信内容分片拆分
     *
     * @param session
     * @param message
     * @param esmClass
     * @param sourceAddress
     * @param destAddress
     * @param registeredDelivery
     * @param dataCoding
     * @param optionalParameter
     * @return
     */
    private static String submitMessage(SmppSession session, byte[] message, byte esmClass, Address sourceAddress, Address destAddress, byte registeredDelivery, byte dataCoding, Tlv... optionalParameter) {
        SubmitSm sms = new SubmitSm();
        sms.setEsmClass(esmClass);
        sms.setSourceAddress(sourceAddress);
        sms.setDestAddress(destAddress);
        sms.setRegisteredDelivery(registeredDelivery);
        sms.setDataCoding(dataCoding);
        try {
            sms.setShortMessage(message);
        } catch (SmppInvalidArgumentException e) {
            e.printStackTrace();
        }
        for (Tlv optional : optionalParameter) {
            sms.setOptionalParameter(optional);
        }
        try {
            SubmitSmResp submitResp = session.submit(sms, 400000L);
            logger.info("Message submitted, message_id is {}", submitResp.getMessageId());
            Assert.assertNotNull(submitResp);
        } catch (RecoverablePduException e) {
            e.printStackTrace();
        } catch (UnrecoverablePduException e) {
            e.printStackTrace();
        } catch (SmppTimeoutException e) {
            //提交超时
            e.printStackTrace();
        } catch (SmppChannelException e) {
            //通道连接异常
            e.printStackTrace();
        } catch (InterruptedException e) {
            //异常中断
            e.printStackTrace();
        }
        return null;
    }

}
