package com.cloudhopper.smpp.demo;

import com.cloudhopper.commons.charset.CharsetUtil;
import com.cloudhopper.commons.util.windowing.WindowFuture;
import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.impl.DefaultSmppClient;
import com.cloudhopper.smpp.impl.DefaultSmppSessionHandler;
import com.cloudhopper.smpp.pdu.*;
import com.cloudhopper.smpp.type.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author joelauer (twitter: @jjlauer or <a href="http://twitter.com/jjlauer" target=window>http://twitter.com/jjlauer</a>)
 */
public class ClientMain2 {
    private static final Logger logger = LoggerFactory.getLogger(ClientMain2.class);
    
//    public static String text160 = "India's new fishing game is open, with a thousand times explosion rate of unlimited shotgun, recharge 1:1, withdraw on the same day. 3000rupees per day, today only. https://bit.ly/3g5KTU2";
//    public static String systemId = "test-mk-91";
//    public static String password = "888888";
//    public static String host = "47.241.3.118";
//    public static Integer port = 7776;
    public static String text160 = "India's new fishing game is open, with a thousand times explosion rate of unlimited shotgun, recharge 1:1, withdraw on the same day. 3000rupees per day, today only. https://bit.ly/3g5KTU2";
    public static String systemId = "bltest";
    public static String password = "bltest";
    public static String host = "47.241.3.118";
    public static Integer port = 7776;

    static public void main(String[] args) throws Exception {
        //
        // setup 3 things required for any session we plan on creating
        //
        
        // for monitoring thread use, it's preferable to create your own instance
        // of an executor with Executors.newCachedThreadPool() and cast it to ThreadPoolExecutor
        // this permits exposing thinks like executor.getActiveCount() via JMX possible
        // no point renaming the threads in a factory since underlying Netty 
        // framework does not easily allow you to customize your thread names
        ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
        
        // to enable automatic expiration of requests, a second scheduled executor
        // is required which is what a monitor task will be executed with - this
        // is probably a thread pool that can be shared with between all client bootstraps
        ScheduledThreadPoolExecutor monitorExecutor = (ScheduledThreadPoolExecutor)Executors.newScheduledThreadPool(1, new ThreadFactory() {
            private AtomicInteger sequence = new AtomicInteger(0);
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setName("SmppClientSessionWindowMonitorPool-" + sequence.getAndIncrement());
                return t;
            }
        });
        
        // a single instance of a client bootstrap can technically be shared
        // between any sessions that are created (a session can go to any different
        // number of SMSCs) - each session created under
        // a client bootstrap will use the executor and monitorExecutor set
        // in its constructor - just be *very* careful with the "expectedSessions"
        // value to make sure it matches the actual number of total concurrent
        // open sessions you plan on handling - the underlying netty library
        // used for NIO sockets essentially uses this value as the max number of
        // threads it will ever use, despite the "max pool size", etc. set on
        // the executor passed in here
        DefaultSmppClient clientBootstrap = new DefaultSmppClient(Executors.newCachedThreadPool(), 1, monitorExecutor);

        //
        // setup configuration for a client session
        // 回调处理器
        //
        DefaultSmppSessionHandler sessionHandler = new ClientSmppSessionHandler();

        SmppSessionConfiguration config0 = new SmppSessionConfiguration();
        config0.setWindowSize(1);
        config0.setName("test-mk");
        config0.setType(SmppBindType.TRANSCEIVER);
        config0.setHost(host);
        config0.setSystemId(systemId);
        config0.setPassword(password);
        config0.setPort(port);
        config0.setConnectTimeout(10000);
        config0.getLoggingOptions().setLogBytes(true);
        // to enable monitoring (request expiration)
        config0.setRequestExpiryTimeout(30000);
        config0.setWindowMonitorInterval(15000);
        config0.setCountersEnabled(true);

        //
        // create session, enquire link, submit an sms, close session
        //
        SmppSession session0 = null;
        

        try {
            // create session a session by having the bootstrap connect a
            // socket, send the bind request, and wait for a bind response
            session0 = clientBootstrap.bind(config0, sessionHandler);
            logger.info("等待连接绑定响应---------------------------------------------------");
            System.out.println("Press any key to send enquireLink #1");
            System.in.read();

            // demo of a "synchronous" enquireLink call - send it and wait for a response
            EnquireLinkResp enquireLinkResp1 = session0.enquireLink(new EnquireLink(), 99999000);
            logger.info("enquire_link_resp #1: commandStatus [" + enquireLinkResp1.getCommandStatus() + "=" + enquireLinkResp1.getResultMessage() + "]");
            
//            System.out.println("Press any key to send enquireLink #2");
//            System.in.read();

            // demo of an "asynchronous" enquireLink call - send it, get a future,
            // and then optionally choose to pick when we wait for it
            WindowFuture<Integer,PduRequest,PduResponse> future0 = session0.sendRequestPdu(new EnquireLink(), 99999000, true);
            if (!future0.await()) {
                logger.error("Failed to receive enquire_link_resp within specified time");
            } else if (future0.isSuccess()) {
                EnquireLinkResp enquireLinkResp2 = (EnquireLinkResp)future0.getResponse();
                logger.info("enquire_link_resp #2: commandStatus [" + enquireLinkResp2.getCommandStatus() + "=" + enquireLinkResp2.getResultMessage() + "]");
            } else {
                logger.error("Failed to properly receive enquire_link_resp: " + future0.getCause());
            }

            System.out.println("Press any key to send submit #1");
            System.in.read();
            SubmitSm submit0 = new SubmitSm();
            
//            String text160 = "Toni Dear user, due to failure of bank card verification, you need to use another "
//            		+ " bank card, and resubmit your application. Sorry for the inconvenience.";
//            byte[] textBytes = CharsetUtil.encode(text160, CharsetUtil.CHARSET_GSM);
//            // address 包括发送方信息，接收方信息
//            submit0.setSourceAddress(new Address((byte) 0x01, (byte) 0x00, "test"));
//            submit0.setShortMessage(textBytes);
//            //接收方号码
//            submit0.setDestAddress(new Address((byte) 0x01, (byte) 0x01, "00919582473827"));

            for (int i = 1; i < 2; i++) {
//                String text160 = "123456";
//                String text160 = "Toni Dear user, due to failure of bank card verification, you need to use another "
//                		+ " bank card, and resubmit your application. Sorry for the inconvenience.";
//            	String text160 = "0065liftare flakewug KreditList Baru-baru ini, platform telah meluncurkan berbagai produk. Jika Anda memiliki permintaan pinjaman, silakan klik http://g16o.com/8/7qoVp untuk mengunduh Aplikasi untuk informasi lebih lanjut";
                byte[] textBytes = CharsetUtil.encode(text160, CharsetUtil.CHARSET_GSM);
                // add delivery receipt
                //submit0.setRegisteredDelivery(SmppConstants.REGISTERED_DELIVERY_SMSC_RECEIPT_REQUESTED);

                // address 包括发送方信息，接收方信息
                submit0.setSourceAddress(new Address((byte) 0x01, (byte) 0x00, "haha"));
                //submit0.setDestAddress(new Address((byte) 0x01, (byte) 0x01, toList.get(1)));//Tk

                // 发送超长消息， 获取使用 getOptionalParameter(Short tag) 方法，而不是 getShortMessage
//                submit0.addOptionalParameter(new Tlv(SmppConstants.TAG_MESSAGE_PAYLOAD, textBytes, "message_payload"));
                // 不拆分正常发送的
                submit0.setShortMessage(textBytes);
                //接收方号码
//                submit0.setDestAddress(new Address((byte) 0x01, (byte) 0x01, "006281218150515"));//Tk
//                submit0.setDestAddress(new Address((byte) 0x01, (byte) 0x01, "00821059108799"));//Tk
                submit0.setDestAddress(new Address((byte) 0x01, (byte) 0x01, "6692473827"));//Tk
                //logger.info("sendWindow.size: {}", session0.getSendWindow().getSize());
                SubmitSmResp submitResp = session0.submit(submit0, 99999000);
                logger.info("submitResponse:{}", submitResp);
            }
                System.out.println("Press any key to unbind and close sessions");
                System.in.read();
            session0.unbind(5000);
        } catch (Exception e) {
            logger.error("", e);
        }

        if (session0 != null) {
            logger.info("Cleaning up session... (final counters)");
            if (session0.hasCounters()) {
                logger.info("tx-enquireLink: {}", session0.getCounters().getTxEnquireLink());
                logger.info("tx-submitSM: {}", session0.getCounters().getTxSubmitSM());
                logger.info("tx-deliverSM: {}", session0.getCounters().getTxDeliverSM());
                logger.info("tx-dataSM: {}", session0.getCounters().getTxDataSM());
                logger.info("rx-enquireLink: {}", session0.getCounters().getRxEnquireLink());
                logger.info("rx-submitSM: {}", session0.getCounters().getRxSubmitSM());
                logger.info("rx-deliverSM: {}", session0.getCounters().getRxDeliverSM());
                logger.info("rx-dataSM: {}", session0.getCounters().getRxDataSM());
            }
            
            session0.destroy();
            // alternatively, could call close(), get outstanding requests from
            // the sendWindow (if we wanted to retry them later), then call shutdown()
        }

        // this is required to not causing server to hang from non-daemon threads
        // this also makes sure all open Channels are closed to I *think*
        logger.info("Shutting down client bootstrap and executors...");
        clientBootstrap.destroy();
        executor.shutdownNow();
        monitorExecutor.shutdownNow();
        
        logger.info("Done. Exiting");
    }

    /**
     * Could either implement SmppSessionHandler or only override select methods
     * by extending a DefaultSmppSessionHandler.
     */
    public static class ClientSmppSessionHandler extends DefaultSmppSessionHandler {

        public ClientSmppSessionHandler() {
            super(logger);
        }

        @Override
        public void firePduRequestExpired(PduRequest pduRequest) {
            logger.warn("PDU request expired: {}", pduRequest);
        }

        @Override
        public PduResponse firePduRequestReceived(PduRequest pduRequest) {
            PduResponse response = pduRequest.createResponse();

            // do any logic here
            
            return response;
        }
        
    }
    
}
