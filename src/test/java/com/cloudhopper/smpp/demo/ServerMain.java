package com.cloudhopper.smpp.demo;

/*
 * #%L
 * ch-smpp
 * %%
 * Copyright (C) 2009 - 2015 Cloudhopper by Twitter
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import cn.hutool.core.util.ObjectUtil;
import com.cloudhopper.commons.charset.CharsetUtil;
import com.cloudhopper.commons.charset.GSMCharset;
import com.cloudhopper.commons.gsm.GsmConstants;
import com.cloudhopper.commons.util.ByteArrayUtil;
import com.cloudhopper.commons.util.HexString;
import com.cloudhopper.commons.util.HexUtil;
import com.cloudhopper.commons.util.windowing.WindowFuture;
import com.cloudhopper.smpp.*;
import com.cloudhopper.smpp.impl.DefaultSmppServer;
import com.cloudhopper.smpp.impl.DefaultSmppSessionHandler;
import com.cloudhopper.smpp.pdu.*;
import com.cloudhopper.smpp.tlv.Tlv;
import com.cloudhopper.smpp.type.*;

import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import com.cloudhopper.smpp.util.DeliveryReceipt;
import com.cloudhopper.smpp.util.DeliveryReceiptException;
import com.cloudhopper.smpp.util.SmppUtil;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author joelauer (twitter: @jjlauer or <a href="http://twitter.com/jjlauer" target=window>http://twitter.com/jjlauer</a>)
 */
public class ServerMain {
    private static final Logger logger = LoggerFactory.getLogger(ServerMain.class);

    private ExecutorService executorServiceForDR = Executors.newFixedThreadPool(50);

    private ExecutorService executorServiceForPdu = Executors.newFixedThreadPool(50);

    /**
     * key smppSession
     * value last heart or request timestamp
     */
    public static ConcurrentHashMap<SmppSession, Long> smppSessionEnquireLink = new ConcurrentHashMap();

    static public void main(String[] args) throws Exception {
        //
        // setup 3 things required for a server
        //

        // for monitoring thread use, it's preferable to create your own instance
        // of an executor and cast it to a ThreadPoolExecutor from Executors.newCachedThreadPool()
        // this permits exposing things like executor.getActiveCount() via JMX possible
        // no point renaming the threads in a factory since underlying Netty 
        // framework does not easily allow you to customize your thread names
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

        // to enable automatic expiration of requests, a second scheduled executor
        // is required which is what a monitor task will be executed with - this
        // is probably a thread pool that can be shared with between all client bootstraps
        ScheduledThreadPoolExecutor monitorExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1, new ThreadFactory() {
            private AtomicInteger sequence = new AtomicInteger(0);

            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setName("SmppServerSessionWindowMonitorPool-" + sequence.getAndIncrement());
                return t;
            }
        });

        // create a server configuration
        SmppServerConfiguration configuration = new SmppServerConfiguration();
        //开放端口 配置连接信息，提供链接
        configuration.setPort(7776);
//        configuration.setPort(3777);
        configuration.setMaxConnectionSize(100);
        configuration.setNonBlockingSocketsEnabled(true);
        configuration.setDefaultRequestExpiryTimeout(30000);
        configuration.setDefaultWindowMonitorInterval(15000);
        configuration.setDefaultWindowSize(100);
        configuration.setDefaultWindowWaitTimeout(configuration.getDefaultRequestExpiryTimeout());
        configuration.setDefaultSessionCountersEnabled(true);
        configuration.setJmxEnabled(true);
        configuration.setInterfaceVersion(SmppConstants.VERSION_3_3);
        // create a server, start it up
        //创建一个SmppServer等待 客户端发起连接
        DefaultSmppServer smppServer = new DefaultSmppServer(configuration, new DefaultSmppServerHandler(), executor, monitorExecutor);

        logger.info("Starting SMPP server...");
        smppServer.start();
        logger.info("SMPP server started");

        System.out.println("Press any key to stop server");
        System.in.read();

        logger.info("Stopping SMPP server...");
        smppServer.stop();
        logger.info("SMPP server stopped");

        logger.info("Server counters: {}", smppServer.getCounters());
    }

    public static class DefaultSmppServerHandler implements SmppServerHandler {

        @Override
        public void sessionBindRequested(Long sessionId, SmppSessionConfiguration sessionConfiguration, final BaseBind bindRequest) throws SmppProcessingException {
            // test name change of sessions
            // this name actually shows up as thread context....
            sessionConfiguration.setName("Application.SMPP." + sessionConfiguration.getName());
            logger.info("开始处理连接绑定请求");

            //throw new SmppProcessingException(SmppConstants.STATUS_BINDFAIL, null);
        }

        /**
         * 创建会话 ，EMCS与 SMSmap
         *
         * @param sessionId            The unique numeric identifier assigned to the bind request.
         *                             Will be the same value between sessionBindRequested, sessionCreated,
         *                             and sessionDestroyed method calls.
         * @param session              The server session associated with the bind request and
         *                             underlying channel.
         * @param preparedBindResponse The prepared bind response that will
         *                             eventually be returned to the client when "serverReady" is finally
         *                             called on the session.
         * @throws SmppProcessingException
         */
        @Override
        public void sessionCreated(Long sessionId, SmppServerSession session, BaseBindResp preparedBindResponse) throws SmppProcessingException {
            logger.info("连接成功，创建会话开始Session created: ID:{}-{}", sessionId, session);
            // need to do something it now (flag we're ready)
            session.serverReady(new TestSmppSessionHandler(session));
        }

        @Override
        public void sessionDestroyed(Long sessionId, SmppServerSession session) {
            logger.info("客户端销毁会话，Session destroyed: ID:{}-{}", sessionId, session);
            // print out final stats
//            if (session.hasCounters()) {
//                logger.info(" final session rx-submitSM: {}", session.getCounters().getRxSubmitSM());
//            }

            // make sure it's really shutdown
            smppSessionEnquireLink.remove(session);
            session.destroy();
        }

    }

    public static void putSessionEnquireLinkTime(SmppSession session) {
        smppSessionEnquireLink.put(session, System.currentTimeMillis());
    }

    public static class TestSmppSessionHandler extends DefaultSmppSessionHandler {

        private WeakReference<SmppSession> sessionRef;

        public TestSmppSessionHandler(SmppSession session) {
            this.sessionRef = new WeakReference<SmppSession>(session);
        }

        @Override
        public boolean firePduReceived(Pdu pdu) {
            // default handling is to accept pdu for processing up chain
            SmppSession session = sessionRef.get();
            putSessionEnquireLinkTime(session);
//            logger.info("rx-enquireLink: {}",session.getCounters().getRxEnquireLink());
            //TODO 某种异常情况下返回 false ,则不走到firePduRequestReceived ,即无响应 迫使客户端进行重连
//            if (session.getConfiguration().getSystemId().equals("KMI62mk")) {
//                return false;
//            }
            return true;
        }



        @Override
        public PduResponse firePduRequestReceived(PduRequest pduRequest) throws SmppInvalidArgumentException {
            SmppSession session = sessionRef.get();
            String systemId = session.getConfiguration().getSystemId();
            logger.info("receiver PDU from systemId :{} host:{} port:{}", systemId, session.getConfiguration().getHost(), session.getConfiguration().getPort());
            String messageId = UUID.randomUUID().toString().replace("-", "");
            //创建相应 response,根据请求类型可以分为
            //1.enquire_link_resp
            //2.submit_sm_resp
            //3.deliver_sm_resp
            PduResponse responsePdu = pduRequest.createResponse();
            //请求是 submit sms
            if (pduRequest instanceof SubmitSm) {
                SubmitSm submitSm = ((SubmitSm) pduRequest);
                boolean userDataHeaderIndicatorEnabled = SmppUtil.isUserDataHeaderIndicatorEnabled(submitSm.getEsmClass());
                String sourceAddr = submitSm.getSourceAddress().getAddress();
                String destination = submitSm.getDestAddress().getAddress();
                byte[] shortMessage = submitSm.getShortMessage();
                byte coding = submitSm.getDataCoding();
                if (userDataHeaderIndicatorEnabled) {
                    //拆分提交进行合并
                    int udhl = (shortMessage[0] & 0xff);
                    String hexString = HexUtil.toHexString(shortMessage, 1 + udhl, shortMessage.length - udhl - 1);
                    logger.info("Receiving submit_sm {} hexString:{},{}, return message id {}",
                            HexUtil.toHexString(shortMessage, 0, 1 + udhl), hexString,
                            new String(shortMessage, 1 + udhl, shortMessage.length - udhl - 1),
                            messageId);
                    String content;
                    if (coding == SmppConstants.DATA_CODING_UCS2) {
                        content = CharsetUtil.decode(HexUtil.toByteArray(hexString), CharsetUtil.CHARSET_UCS_2);
                    } else {
                        content = CharsetUtil.decode(HexUtil.toByteArray(hexString), CharsetUtil.CHARSET_GSM);
                    }
                    logger.info("转义内容:{}", content);
                    //返回 submitResponse
                    if (responsePdu instanceof SubmitSmResp) {
                        SubmitSmResp submitSmResp = ((SubmitSmResp) responsePdu);
                        //返回MessageId
                        submitSmResp.setMessageId(messageId);
                        submitSmResp.setCommandStatus(0);
                    }
                    // mimic how long processing could take on a slower smsc
                    try {
//                        Thread.sleep(20000000);
                        session.sendResponsePdu(responsePdu);
                    } catch (RecoverablePduException e) {
                        e.printStackTrace();
                    } catch (UnrecoverablePduException e) {
                        e.printStackTrace();
                    } catch (SmppChannelException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    responsePdu = null;
                } else {
                    byte[] messages;
                    //判断编码类型
                    String smsContent = null;
                    if (submitSm.getShortMessage() != null && submitSm.getShortMessage().length > 0) {
                        messages = shortMessage;
                    } else {
                        //TLV 长短信提交
                        Tlv tlv = submitSm.getOptionalParameter(SmppConstants.TAG_MESSAGE_PAYLOAD);
                        messages = tlv.getValue();
                    }
                    switch (submitSm.getDataCoding()) {
                        //UCS_2
                        case SmppConstants.DATA_CODING_UCS2:
                            smsContent = CharsetUtil.decode(messages, CharsetUtil.CHARSET_UCS_2);
                            break;
                        default:
                            //默认 GSM
                            smsContent = CharsetUtil.decode(messages, CharsetUtil.CHARSET_GSM);
                            break;
                    }

                    logger.info("senderId:{},phone:{},smsContent:{},dataCoding:{}", sourceAddr, destination, smsContent, coding);
                    //返回 submitResponse
                    if (responsePdu instanceof SubmitSmResp) {
                        SubmitSmResp submitSmResp = ((SubmitSmResp) responsePdu);
                        //返回MessageId
                        submitSmResp.setMessageId(messageId);
                    }
                    // mimic how long processing could take on a slower smsc
                    try {
//                        Thread.sleep(40000);
                        session.sendResponsePdu(responsePdu);
                    } catch (RecoverablePduException e) {
                        e.printStackTrace();
                    } catch (UnrecoverablePduException e) {
                        e.printStackTrace();
                    } catch (SmppChannelException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    responsePdu = null;
                }
            }


            //如果是 submit sms 模拟直接返回 dr
            if (pduRequest instanceof SubmitSm) {
                SubmitSm submitSm = ((SubmitSm) pduRequest);
                try {
                    DeliveryReceipt dlr = DeliveryReceipt.parseShortMessage("sub:001 id:4 dlvrd:001 done date:1006020051 stat:DELIVRD submit date:1006020051 err:650 text:", DateTimeZone.UTC, true);
                    dlr.setMessageId(messageId);
                    dlr.setState(SmppConstants.STATE_UNDELIVERABLE);
                    dlr.setErrorCode(650);
                    sendDeliveryReceipt(session, submitSm.getDestAddress(), submitSm.getSourceAddress(), submitSm.getDataCoding(), dlr.toShortMessage().getBytes());
                } catch (DeliveryReceiptException e) {
                    e.printStackTrace();
                } catch (SmppInvalidArgumentException e) {
                    e.printStackTrace();
                }
            }
            return responsePdu;
        }
    }

    private static void sendDeliveryReceipt(SmppSession session, Address mtDestinationAddress, Address mtSourceAddress, byte dataCoding, byte[] shoreMessage) throws SmppInvalidArgumentException {

        DeliverSm deliver = new DeliverSm();
        deliver.setEsmClass(SmppConstants.ESM_CLASS_MT_SMSC_DELIVERY_RECEIPT);
        deliver.setSourceAddress(mtDestinationAddress);
        deliver.setDestAddress(mtSourceAddress);
        deliver.setDataCoding(dataCoding);
        deliver.setShortMessage(shoreMessage);
        sendRequestPdu(session, deliver);
    }

    private static void sendRequestPdu(SmppSession session, DeliverSm deliver) {
        try {
            WindowFuture<Integer, PduRequest, PduResponse> future = session.sendRequestPdu(deliver, 2, false);
            if (!future.await()) {
                logger.info("[sendRequestPdu] seqNumber:{}, deliver_sm:{}, Failed to receive deliver_sm_resp within specified time",deliver.getSequenceNumber(),deliver);
            } else if (future.isSuccess()) {
                DeliverSmResp deliverSmResp = (DeliverSmResp) future.getResponse();
                logger.info("deliver_sm_resp: commandStatus [" + deliverSmResp.getCommandStatus() + "=" + deliverSmResp.getResultMessage() + "]");
            } else {
                logger.error("Failed to properly receive deliver_sm_resp: " + future.getCause());
            }
        } catch (Exception e) {
        }
    }


}
