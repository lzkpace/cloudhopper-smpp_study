package com.cloudhopper.smpp.util;

/*
 * #%L
 * ch-smpp
 * %%
 * Copyright (C) 2009 - 2015 Cloudhopper by Twitter
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

// third party imports
import ch.qos.logback.core.encoder.ByteArrayUtil;
import cn.hutool.core.util.HexUtil;
import com.cloudhopper.commons.charset.CharsetUtil;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.pdu.BufferHelper;
import com.cloudhopper.smpp.type.Address;
import org.jboss.netty.buffer.ChannelBuffer;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OctetString;

import java.util.Random;

// my imports

/**
 *
 * @author joelauer (twitter: @jjlauer or <a href="http://twitter.com/jjlauer" target=window>http://twitter.com/jjlauer</a>)
 */
public class PduUtilTest {
    private static final Logger logger = LoggerFactory.getLogger(PduUtilTest.class);

    @Test
    public void isRequestCommandId() throws Exception {
        Assert.assertEquals(true, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_BIND_TRANSMITTER));
        Assert.assertEquals(true, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_BIND_RECEIVER));
        Assert.assertEquals(true, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_BIND_TRANSCEIVER));
        Assert.assertEquals(true, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_CANCEL_SM));
        Assert.assertEquals(true, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_DATA_SM));
        Assert.assertEquals(true, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_ENQUIRE_LINK));
        Assert.assertEquals(true, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_SUBMIT_SM));
        Assert.assertEquals(true, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_DELIVER_SM));
        Assert.assertEquals(true, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_UNBIND));
        Assert.assertEquals(false, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_GENERIC_NACK));
        Assert.assertEquals(false, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_BIND_TRANSMITTER_RESP));
        Assert.assertEquals(false, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_BIND_RECEIVER_RESP));
        Assert.assertEquals(false, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_BIND_TRANSCEIVER_RESP));
        Assert.assertEquals(false, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_CANCEL_SM_RESP));
        Assert.assertEquals(false, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_DATA_SM_RESP));
        Assert.assertEquals(false, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_ENQUIRE_LINK_RESP));
        Assert.assertEquals(false, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_SUBMIT_SM_RESP));
        Assert.assertEquals(false, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_DELIVER_SM_RESP));
        Assert.assertEquals(false, PduUtil.isRequestCommandId(SmppConstants.CMD_ID_UNBIND_RESP));
    }

    @Test
    public void isResponseCommandId() throws Exception {
        Assert.assertEquals(false, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_BIND_TRANSMITTER));
        Assert.assertEquals(false, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_BIND_RECEIVER));
        Assert.assertEquals(false, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_BIND_TRANSCEIVER));
        Assert.assertEquals(false, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_CANCEL_SM));
        Assert.assertEquals(false, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_DATA_SM));
        Assert.assertEquals(false, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_ENQUIRE_LINK));
        Assert.assertEquals(false, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_SUBMIT_SM));
        Assert.assertEquals(false, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_DELIVER_SM));
        Assert.assertEquals(false, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_UNBIND));
        Assert.assertEquals(true, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_GENERIC_NACK));
        Assert.assertEquals(true, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_BIND_TRANSMITTER_RESP));
        Assert.assertEquals(true, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_BIND_RECEIVER_RESP));
        Assert.assertEquals(true, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_BIND_TRANSCEIVER_RESP));
        Assert.assertEquals(true, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_CANCEL_SM_RESP));
        Assert.assertEquals(true, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_DATA_SM_RESP));
        Assert.assertEquals(true, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_ENQUIRE_LINK_RESP));
        Assert.assertEquals(true, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_SUBMIT_SM_RESP));
        Assert.assertEquals(true, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_DELIVER_SM_RESP));
        Assert.assertEquals(true, PduUtil.isResponseCommandId(SmppConstants.CMD_ID_UNBIND_RESP));
    }

    @Test
    public void calculateByteSizeOfAddress() throws Exception {
        Assert.assertEquals(3, PduUtil.calculateByteSizeOfAddress(null));
        Assert.assertEquals(3, PduUtil.calculateByteSizeOfAddress(new Address()));
        Assert.assertEquals(4, PduUtil.calculateByteSizeOfAddress(new Address((byte)0x01, (byte)0x01, "A")));
    }

    @Test
    public void calculateByteSizeOfNullTerminatedString() throws Exception {
        Assert.assertEquals(1, PduUtil.calculateByteSizeOfNullTerminatedString(null));
        Assert.assertEquals(1, PduUtil.calculateByteSizeOfNullTerminatedString(""));
        Assert.assertEquals(2, PduUtil.calculateByteSizeOfNullTerminatedString("A"));
    }

    @Test
    public void encodeSms(){
        String content = "1العمل من المنزل،الدخل اليومي 1K-7Kلبرجاء النقر فوق: wa.me/85294265839";
        System.out.println(Ucs2.isUcs2Encodable(content));

        byte[] sms = CharsetUtil.encode(content, CharsetUtil.CHARSET_UCS_2);
        try {
            byte[] s = BufferHelper.createByteArray(BufferHelper.createBuffer("201B3C4164614D6F64616C1B3E204B6F6465205665726966696B61736920616E64613A203238383733312C2077616B7475206566656B7469663A203330206D656E6974"));
            ChannelBuffer channelBuffer = BufferHelper.createBuffer(s);
            System.out.println(s[0]& 0xff);
            System.out.println(BufferHelper.createHexString(channelBuffer));
            System.out.println("=====LATIN1");
            System.out.println(CharsetUtil.decode(s, CharsetUtil.CHARSET_ISO_8859_1));
            System.out.println("=====GSM");
            System.out.println(CharsetUtil.decode(s, CharsetUtil.CHARSET_GSM));
            System.out.println("=====UCS_2");
            System.out.println(CharsetUtil.decode(s, CharsetUtil.CHARSET_UCS_2));
            System.out.println("=====NAME_VFD2_GSM");
            System.out.println(CharsetUtil.decode(sms, CharsetUtil.NAME_VFD2_GSM));
            System.out.println("=====ascii");
            System.out.println(HexUtil.decodeHexStr("005B005400650065006E00500061007400740069002000760075006E0067006F005D0020004100A0006800690067006800A00061006D006F0075006E007400A00069007300A000610062006F0075007400A00074006F00A0006500780070006900720065002C00A000670065007400A00069007400A0006E006F007700A00061006E006400A0006D0061006B006500A00079006F0075007200A0006C00690066006500A00062006500740074006500720021002000680074007400700073003A002F002F006200690074002E006C0079002F006F00370035003100A00044004F0054"));
            OctetString aPublic = new OctetString("A");
            System.out.println(HexUtil.encodeHex(aPublic.getValue()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void UDH(){
        int i = new Random().nextInt();
        System.out.println(i);
        byte[] bytes = new byte[]{(byte) 0000001};
        System.out.println(i);
        System.out.println(ByteArrayUtil.toHexString(bytes));
        System.out.println(bytes[0]);

    }
}
