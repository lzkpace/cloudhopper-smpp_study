package com.cloudhopper.smpp.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 * 何桂明
 */
public class DateFormatUtil {

    public static final DateFormat yMdTHmsSformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    public static final DateFormat yyMdHmsSformat = new SimpleDateFormat("yyMMddHHmmssSS");
    public static final DateFormat yyMdHmsformat = new SimpleDateFormat("yyyyMMddhhmmss");
    public static final DateFormat HHformat = new SimpleDateFormat("HH");
    public static final DateFormat yMdformat = new SimpleDateFormat("yyyy-MM-dd");
    public static final DateFormat yMdHformat = new SimpleDateFormat("yyyy-MM-dd HH");
    public static final DateFormat y_M_d_H_m_sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    // 14/10/2018 13:25
    public static final DateFormat dMyHmHformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public static final DateFormat yyMdformat = new SimpleDateFormat("yyyyMMdd");

    public static final SimpleDateFormat orderIdFormatter = new SimpleDateFormat("yyyyMMddhhmmssSSS");

    public static DateTimeFormatter y_M_dFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static DateTimeFormatter y_M_d_H_m_sFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static DateTimeFormatter HHFormatter = DateTimeFormatter.ofPattern("HH");
    public static DateTimeFormatter yMdHmFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
    public static DateTimeFormatter yMdHmsFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    public static DateTimeFormatter yMdHmsSFormatter = DateTimeFormatter.ofPattern("yyyyMMddhhmmssSSS");
    public static DateTimeFormatter yMdHMformat = DateTimeFormatter.ofPattern("yyMMddHHmmss");
    public static String format(DateFormat dateFormat, Date date) {
        try {
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String format(DateFormat dateFormat, Date date, int addDay) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_MONTH, addDay);

            return dateFormat.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取今天的时间-服务器上时间为UTC时间，本地时间为UTC+7时间
     * @return
     */
    public static String getNowUtcDate(DateTimeFormatter formatter) {
        ZoneId zoneId = ZoneId.of("UTC+0");
        ZonedDateTime dd = ZonedDateTime.now(zoneId);
        return formatter.format(dd);
    }

    /**
     * 获取今天的时间-服务器上时间为UTC时间，本地时间为UTC+7时间
     * @return
     */
    public static String getNowDate(DateTimeFormatter formatter) {
        ZoneId zoneId = ZoneId.of("UTC+7");
        ZonedDateTime dd = ZonedDateTime.now(zoneId);
        return formatter.format(dd);
    }

    /**
     * 获取当前时间-服务器上时间为UTC时间，本地时间为UTC+7时间
     * @return
     */
    public static ZonedDateTime getNowZonedDateTime() {
        ZoneId zoneId = ZoneId.of("UTC+7");
        ZonedDateTime dd = ZonedDateTime.now(zoneId);
        return dd;
    }

    /**
     * 获取当前的Date类型日期
     * @return
     */
    public static Date getNowDate() {
        Date date = null;
        try {
            ZoneId zoneId = ZoneId.of("UTC+7");
            ZonedDateTime dd = ZonedDateTime.now(zoneId);
            LocalDateTime localDateTime = dd.toLocalDateTime();

            date = yMdTHmsSformat.parse(localDateTime.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    /**
     * 两个日期是否是同一天
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isSameDay(Date date1, Date date2) {
        if (null != date1 && null != date2) {
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(date1);

            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(date2);

            return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                    && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)
                    && cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH);
        }
        return false;
    }

    public static Date parse(DateFormat yyMdHmsSformat, String time) {
        try {
            return yyMdHmsSformat.parse(time);
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * 获取vacode过期时间
     * @return
     */
    public static Date getExpireDateForVACode(){
        long currentTime = System.currentTimeMillis() ;
        //增加6个小时
        currentTime += 6 * 60 * 60 * 1000;
        return new Date(currentTime);
    }

    /**
     * 判定日期是否符合格式
     * @param dateString
     * @param formatPattern 例如 yyyy/MM/dd HH:mm:ss
     * @return
     */
    public static boolean isValidDate(String dateString,String formatPattern) {
        boolean convertSuccess = true;
        // 指定日期格式为四位年/两位月份/两位日期，注意yyyy/MM/dd区分大小写；
        SimpleDateFormat format = new SimpleDateFormat(formatPattern);
        try {
            // 设置lenient为false.
            // 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(dateString);
        } catch (ParseException e) {
            // e.printStackTrace();
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            convertSuccess = false;
        }
        return convertSuccess;
    }


    /**
     * 比较日期大小
     * @param DATE1
     * @param DATE2
     * @param pattern
     * @return
     */
    public static int compareDate(String DATE1, String DATE2,String pattern) {
        DateFormat df = new SimpleDateFormat(pattern);
        try {
            Date dt1 = df.parse(DATE1);
            Date dt2 = df.parse(DATE2);
            if (dt1.getTime() > dt2.getTime()) {
                System.out.println("dt1 在dt2前");
                return 1;
            } else if (dt1.getTime() < dt2.getTime()) {
                System.out.println("dt1在dt2后");
                return -1;
            } else {
                return 0;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取当前日期
     * @return
     */
    public static String getCurrentDateString() {
        return format(yMdformat,new Date());
    }


    public static void main(String[] args){
//        Date date = getExpireDateForVACode();
//        System.out.println(date+"");

        System.out.println(getNowDate(yMdHMformat));
    }
}
